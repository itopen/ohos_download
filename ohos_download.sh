#!/usr/bin/env bash

set -e

source ./main_menu.sh

ROOT_DIR=$(cd $(dirname $0); pwd)
param_file=${ROOT_DIR}/product.list
config_file=${ROOT_DIR}/.config

function show_download_info() {
  echo
  echo -e "================================================================================================"
  if [[ ${code_branch} == "-" ]]; then
    echo -e "\033[32;40myou have been download ${product} code\033[0m"
    echo -e "url       : \033[32;40m${manifest_url}\033[0m"
    echo -e "xml_name  : \033[32;40m${manifest_xml}\033[0m"
    echo -e "code_path : \033[32;40m${code_path}\033[0m"
    echo -e "init   cmd: \033[32;40m${repo_init}\033[0m"
    echo -e "sync   cmd: \033[32;40mrepo sync -c\033[0m"
    echo -e "lfs    cmd: \033[32;40mrepo forall -c 'git lfs pull'\033[0m"
  else
    echo -e "\033[32;40myou have been download ${product} ${code_branch} code\033[0m"
    echo -e "url       : \033[32;40m${manifest_url}\033[0m"
    echo -e "branch    : \033[32;40m${code_branch}\033[0m"
    echo -e "xml_name  : \033[32;40m${manifest_xml}\033[0m"
    echo -e "code_path : \033[32;40m${code_path}\033[0m"
    echo -e "init   cmd: \033[32;40m${repo_init}\033[0m"
    echo -e "sync   cmd: \033[32;40mrepo sync -c\033[0m"
    echo -e "lfs    cmd: \033[32;40mrepo forall -c 'git lfs pull'\033[0m"
    echo -e "set_br cmd: \033[32;40mrepo start ${code_branch} --all\033[0m"
  fi
  if [[ ${manifest_xml} == "default_weekly_0905.xml" ]]; then
    echo -e "extern cmd: \033[32;40m./build/prebuilts_download.sh\033[0m"
    echo -e "extern cmd: \033[32;40mrm -rf ./prebuilts/rustc\033[0m"
    echo -e "extern cmd: \033[32;40mgit clone -b weekly_20230905 https://gitee.com/riscv-sig/rustc.git prebuilts/rustc\033[0m"
  fi
  echo -e "================================================================================================"
  echo
}

function repo_check() {
  set +e
  bash -c "which repo >> /dev/null 2>&1"
  if [[ $? -ne 0 ]]; then
    set -e
    echo "repo instll..."
    bash -c "curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > repo"
    bash -c "chmod a+x repo"
    bash -c "mkdir -p ~/bin"
    bash -c "mv repo ~/bin"
    export PATH=~/bin:$PATH
  fi
}

function download_code() {
  repo_check

  if [ -d ${code_path} ]; then
    code_path=${code_path}_tmp
    if [ -d ${code_path} ]; then
      echo "you have already exist following path:"
      echo "${code_path:0:-4}"
      echo "${code_path}"
      echo "please input the path name you want to download code"
      echo "if you want to delete the ${code_path} directly and then download it again, please press Enter."
      read path_name
      if [[ $path_name == "" ]]; then
        rm -rf ${code_path:?}
      else
        code_path=${base_dir}/${path_name}
        if [ -d ${code_path} ]; then
          echo "path: ${code_path} already exist!!! process exit"
          exit 1
        fi
      fi
    fi
  fi

  mkdir -p ${code_path}

  pushd ${code_path}
  bash -c "${repo_init}"
  set +e
  bash -c "repo sync -c"
  bash -c "repo forall -c 'git lfs pull'"
  if [[ ${code_branch} != "-" ]]; then
    bash -c "repo start ${code_branch} --all"
  fi
  set -e
  bash -c "repo sync -c"
  bash -c "repo forall -c 'git lfs pull'"
  if [[ ${code_branch} != "-" ]]; then
    bash -c "repo start ${code_branch} --all"
  fi
  if [[ ${manifest_xml} == "default_weekly_0905.xml" ]]; then
    bash -c "./build/prebuilts_download.sh"
    rm -rf ./prebuilts/rustc
    git clone -b weekly_20230905 https://gitee.com/riscv-sig/rustc.git prebuilts/rustc
  fi
  popd
  show_download_info
}

function base_dir_check() {
  clear
  if [ ! -f ${config_file} ]; then
    echo "first download code, please set code download path, default is ~/OpenHarmony"
    read base_dir
    if [[ ${base_dir} == "" ]]; then
      base_dir=$(cd ~ && pwd)/OpenHarmony
    elif [[ ${base_dir:0:1} == "." ]]; then
      base_dir=${ROOT_DIR}${base_dir:1}
    elif [[ ${base_dir:0:1} == "~" ]]; then
        base_dir=$(cd ~ && pwd)${base_dir:1}
    fi
    echo ${base_dir} > ${config_file}
  fi
  export base_dir=$(cat ${config_file})
  if [[ ${base_dir} == "" ]]; then
    echo "base dir is null"
    exit 1
  fi
}

function main() {
  export code_path=${base_dir}/${path_name}

  if [[ ${manifest_url} == "" ]]; then
    echo "download code has not config"
    return 0
  fi

  download_code
  echo "download code success ^_^"
}

function args_check() {
  main_menu $1
  read version
  
  while read line
  do
    if [[ ${line} == "" || ${line:0:1} == "#" ]]; then
      continue
    fi
    if [[ ${version} == $(echo ${line} | awk '{print $1}') ]]; then
      break
    fi
  done < ${param_file}

  if [[ $(echo ${line} | awk '{print $1}') == "" ]]; then
    echo "version is not exist!"
    return 0
  fi

  export product=$(echo ${line} | awk '{print $2}')
  export path_name=$(echo ${line} | awk '{print $3}')
  export code_branch=$(echo ${line} | awk '{print $4}')
  export manifest_url=$(echo ${line} | awk '{print $5}')
  export manifest_branch=$(echo ${line} | awk '{print $6}')
  export manifest_xml=$(echo ${line} | awk '{print $7}')
  export repo_init="repo init -u ${manifest_url} -b ${manifest_branch} -m ${manifest_xml} --no-repo-verify"
  if [[ ${manifest_url} == "-" ]]; then
    echo "download code has not config"
    return 0
  fi
  if [[ ${product:0:7} == "android" ]]; then
    echo "product is android issue, product is ${product}"
    export base_dir=$(cd ~ && pwd)/android/aosp
  fi
  main
  return 0
}

base_dir_check
args_check $1
