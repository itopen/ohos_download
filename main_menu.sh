#!/usr/bin/env bash

char_flag="*"
char_num=44

front_black='30'
front_red='31'
front_green='32'
front_yellow='33'
front_blue='34'
front_purple='35'
front_cyan='36'
front_white='37'

back_black='40'
back_red='41'
back_green='42'
back_yellow='43'
back_blue='44'
back_purple='45'
back_cyan='46'
back_white='47'

font_underline='04'
color_flash='05'

color_prefix='\e['
color_suffix='\e[0m'
color_sep=';'
color_flag='m'

function openharmony_riscv_menu() {
  purple_black=${color_prefix}${front_purple}${color_sep}${back_black}${color_flag}
  echo -e "* ${purple_black}OpenHarmony Riscv64    ${color_suffix}                  *"
  echo -e "*   dayu800-ohos               press 1.1   *"
  echo -e "*   dayu800-sig                press 1.2   *"
}

function openharmony_arm64_menu() {
  yellow_black=${color_prefix}${front_yellow}${color_sep}${back_black}${color_flag}
  echo -e "* ${yellow_black}OpenHarmony Arm64      ${color_suffix}                  *"
  echo -e "*   raspberry-v4.1-release     press 1.100 *"
  echo -e "*   raspberry-v5.0-release     press 1.101 *"
}

function openharmony_branch_menu() {
  cyan_black=${color_prefix}${front_cyan}${color_sep}${back_black}${color_flag}
  echo -e "* ${cyan_black}OpenHarmony Branch${color_suffix}                       *"
  echo -e "*   OpenHarmony master         prese 2.1   *"
  echo -e "*   OpenHarmony-3.0-LTS        press 2.2   *"
  echo -e "*   OpenHarmony-3.1-Release    press 2.3   *"
  echo -e "*   OpenHarmony-3.2-Release    press 2.4   *"
  echo -e "*   OpenHarmony-4.0-Release    press 2.5   *"
  echo -e "*   OpenHarmony-4.1-Release    press 2.6   *"
  echo -e "*   OpenHarmony-5.0.0-Release  press 2.7   *"
  echo -e "*   OpenHarmony-5.0.1-Release  press 2.8   *"
  echo -e "*   OpenHarmony-5.0.2-Release  press 2.9   *"
  echo -e "*   OpenHarmony-5.0.3-Release  press 2.10  *"
}

function openharmony_tag_menu() {
  green_black=${color_prefix}${front_green}${color_sep}${back_black}${color_flag}
  echo -e "* ${green_black}OpenHarmony Tag${color_suffix}                          *"
  echo -e "*   OpenHarmony-v3.0-LTS       press 3.10  *"
  echo -e "*   OpenHarmony-v3.1-Release   press 3.20  *"
  echo -e "*   OpenHarmony-v3.2-Release   press 3.21  *"
  echo -e "*   OpenHarmony-v3.2.1-Release press 3.22  *"
  echo -e "*   OpenHarmony-v3.2.2-Release press 3.23  *"
  echo -e "*   OpenHarmony-v3.2.3-Release press 3.24  *"
  echo -e "*   OpenHarmony-v3.2.4-Release press 3.25  *"
  echo -e "*   OpenHarmony-v4.0-Release   press 3.30  *"
  echo -e "*   OpenHarmony-v4.0.1-Release press 3.31  *"
  echo -e "*   OpenHarmony-v4.0.2-Release press 3.32  *"
  echo -e "*   OpenHarmony-v4.1-Release   press 3.33  *"
  echo -e "*   OpenHarmony-v4.1.1-Release press 3.34  *"
  echo -e "*   OpenHarmony-v4.1.2-Release press 3.35  *"
  echo -e "*   OpenHarmony-v5.0.0-Release press 3.36  *"
  echo -e "*   OpenHarmony-v5.0.1-Release press 3.37  *"
  echo -e "*   OpenHarmony-v5.0.2-Release press 3.38  *"
}

function openharmony_llvm_menu() {
  yellow_black=${color_prefix}${front_yellow}${color_sep}${back_black}${color_flag}
  echo -e "* ${yellow_black}OpenHarmony LLVM${color_suffix}                         *"
  echo -e "*   llvm-master                press 4.1   *"
}

function study_llvm_menu() {
  blue_black=${color_prefix}${front_blue}${color_sep}${back_black}${color_flag}
  echo -e "* ${blue_black}Study LLVM${color_suffix}                               *"
  echo -e "*   llvm-master-study          press 5.1   *"
  echo -e "*   llvm-20240612              press 5.2   *"
  echo -e "*   llvm-20240612-study        press 5.3   *"
}

function main_menu() {
  debug=$1
  # 打印头
  printf "%-${char_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"

  red_flash=${color_prefix}${front_red}${color_sep}${color_flash}${color_flag}
  # 打印欢迎语句
  echo -e "*  ${red_flash}Welcome to download OpenHarmony Code${color_suffix}    *"
  echo -e "* Please Choice OpenHarmony SDK:           *"
  openharmony_riscv_menu
  openharmony_arm64_menu
  openharmony_branch_menu
  openharmony_tag_menu
  openharmony_llvm_menu
  if [[ ${debug} == "debug" ]]; then
    study_llvm_menu
  fi
  # 打印尾
  printf "%-${char_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"
}
