# ohos_download

## 一、脚本介绍

### 1.1 概述

本仓脚本用于项目中下载`ohos`主要分支和tag代码使用，省去下载代码时找对应分支的命令麻烦

### 1.2 路径配置

因每个人当第一次使用该脚本下载代码的时候会提示设置代码下载的路径，然后会将该路径保存在本地的`.config`文件中，后面再次下载则不再提示

- 不输入直接回车，则默认保存在`~/OpenHarmony`里面
- 输入设置`.`，则表示保存在脚本所在的目录下面
- 其他路径可自行设置，但必须为**绝对路径**

运行效果如下：

```shell
first download code, please set code download path, default is ~/OpenHarmony
. # 这里输入想存放的路径
```

### 1.3 分支介绍

脚本运行后如下所示，后续添加其他版本可能有增加，以实际效果为准

```shell
********************************************
*  Welcome to download OpenHarmony Code    *
* Please Choice OpenHarmony SDK:           *
* OpenHarmony Riscv64                      *
*   dayu800-ohos               press 1.1   *
*   dayu800-sig                press 1.2   *
* OpenHarmony Arm64                        *
*   raspberry-v4.1-release     press 1.100 *
*   raspberry-v5.0-release     press 1.101 *
* OpenHarmony Branch                       *
*   OpenHarmony master         prese 2.1   *
*   OpenHarmony-3.0-LTS        press 2.2   *
*   OpenHarmony-3.1-Release    press 2.3   *
*   OpenHarmony-3.2-Release    press 2.4   *
*   OpenHarmony-4.0-Release    press 2.5   *
*   OpenHarmony-4.1-Release    press 2.6   *
*   OpenHarmony-5.0.0-Release  press 2.7   *
*   OpenHarmony-5.0.1-Release  press 2.8   *
*   OpenHarmony-5.0.2-Release  press 2.9   *
*   OpenHarmony-5.0.3-Release  press 2.10  *
* OpenHarmony Tag                          *
*   OpenHarmony-v3.0-LTS       press 3.10  *
*   OpenHarmony-v3.1-Release   press 3.20  *
*   OpenHarmony-v3.2-Release   press 3.21  *
*   OpenHarmony-v3.2.1-Release press 3.22  *
*   OpenHarmony-v3.2.2-Release press 3.23  *
*   OpenHarmony-v3.2.3-Release press 3.24  *
*   OpenHarmony-v3.2.4-Release press 3.25  *
*   OpenHarmony-v4.0-Release   press 3.30  *
*   OpenHarmony-v4.0.1-Release press 3.31  *
*   OpenHarmony-v4.0.2-Release press 3.32  *
*   OpenHarmony-v4.1-Release   press 3.33  *
*   OpenHarmony-v4.1.1-Release press 3.34  *
*   OpenHarmony-v4.1.2-Release press 3.35  *
*   OpenHarmony-v5.0.0-Release press 3.36  *
*   OpenHarmony-v5.0.1-Release press 3.37  *
*   OpenHarmony-v5.0.2-Release press 3.38  *
* OpenHarmony LLVM                         *
*   llvm-master                press 4.1   *
********************************************
```

- **OpenHarmony Riscv64表示下载OpenHarmony-Riscv64的代码**
  - `dayu800-ohos`：表示`OpenHarmony`官方`OpenHarmony-3.2-Release`分支适配dayu800代码，随着官方分支代码更新有可能编译不过
  - `dayu800-sig`：基于`OpenHarmony`官方`OpenHarmony-3.2-Release`分支适配的dayu800代码同步到`riscv-sig`组织，可以编译运行

- **OpenHarmony Branch表示下载官方的主要分支代码**
  - `OpenHarmony master`：表示`OpenHarmony`官方`master`分支源代码
  - `OpenHarmony-3.0-LTS`：表示`OpenHarmony`官方`OpenHarmony-3.0-LTS`分支源代码
  - `OpenHarmony-3.1-Release`：表示`OpenHarmony`官方`OpenHarmony-3.1-Release`分支源代码
  - `OpenHarmony-3.2-Release`：表示`OpenHarmony`官方`OpenHarmony-3.2-Release`分支源代码
  - `OpenHarmony-4.0-Release`：表示`OpenHarmony`官方`OpenHarmony-4.0-Release`分支源代码
  - `OpenHarmony-4.1-Release`：表示`OpenHarmony`官方`OpenHarmony-4.1-Release`分支源代码
  - `OpenHarmony-5.0.0-Release`：表示`OpenHarmony`官方`OpenHarmony-5.0.0-Release`分支源代码
  - `OpenHarmony-5.0.1-Release`：表示`OpenHarmony`官方`OpenHarmony-5.0.1-Release`分支源代码
  - `OpenHarmony-5.0.2-Release`：表示`OpenHarmony`官方`OpenHarmony-5.0.2-Release`分支源代码
  - `OpenHarmony-5.0.3-Release`：表示`OpenHarmony`官方`OpenHarmony-5.0.3-Release`分支源代码
  
  ![icon-note.gif](./img/icon/icon-note.gif) **说明:** x表示临时使用，一般有新的beta版本会替换成新版本
  
- **OpenHarmony Tag表示下载官方的主要Tag代码**

  - `OpenHarmony-v3.0-LTS`：表示`OpenHarmony`官方`OpenHarmony-v3.0-LTS`的tag源代码
  - `OpenHarmony-v3.1-Release`：表示`OpenHarmony`官方`OpenHarmony-v3.1-Release`的tag源代码
  - `OpenHarmony-v3.2-Release`：表示`OpenHarmony`官方`OpenHarmony-v3.2-Release`的tag源代码
  - `OpenHarmony-v3.2.1-Release`：表示`OpenHarmony`官方`OpenHarmony-v3.2.1-Release`的tag源代码
  - `OpenHarmony-v3.2.2-Release`：表示`OpenHarmony`官方`OpenHarmony-v3.2.2-Release`的tag源代码
  - `OpenHarmony-v3.2.3-Release`：表示`OpenHarmony`官方`OpenHarmony-v3.2.3-Release`的tag源代码
  - `OpenHarmony-v3.2.4-Release`：表示`OpenHarmony`官方`OpenHarmony-v3.2.4-Release`的tag源代码
  - `OpenHarmony-v4.0-Release`：表示`OpenHarmony`官方`OpenHarmony-v4.0-Release`的tag源代码
  - `OpenHarmony-v4.0.1-Release`：表示`OpenHarmony`官方`OpenHarmony-v4.0.1-Release`的tag源代码
  - `OpenHarmony-v4.1-Release`：表示`OpenHarmony`官方`OpenHarmony-v4.1-Release`的tag源代码
  - `OpenHarmony-v4.1.1-Release`：表示`OpenHarmony`官方`OpenHarmony-v4.1.1-Release`的tag源代码
  - `OpenHarmony-v4.1.2-Release`：表示`OpenHarmony`官方`OpenHarmony-v4.1.2-Release`的tag源代码
  - `OpenHarmony-v5.0.0-Release`：表示`OpenHarmony`官方`OpenHarmony-v5.0.0-Release`的tag源代码
  - `OpenHarmony-v5.0.1-Release`：表示`OpenHarmony`官方`OpenHarmony-v5.0.1-Release`的tag源代码
  - `OpenHarmony-v5.0.2-Release`：表示`OpenHarmony`官方`OpenHarmony-v5.0.2-Release`的tag源代码

- **OpenHarmony LLVM表示下载官方的LLVM代码**
  - `llvm-master`：表示`OpenHarmony`官方`llvm`工具链`master`分支源代码

- **Study LLVM表示下载学习LLVM的代码**

  - `llvm-master-study`：表示`fork`过来的`OpenHarmony`官方`llvm`工具链`master`分支源代码
  - `llvm-20240612`：表示`OpenHarmony`官方2024年6月12日`llvm`工具链`maste`分支源代码
  - `llvm-20240612-study2`：表示`OpenHarmony`官方2024年6月12日`maste`分支源代码同时用于学习使用的分支

### 1.4 下载介绍

脚本会在1.2 配置的路径下创建对应的代码版本路径，如果对应的代码版本路径已经存在则会生成一个带`_tmp`后缀的路径，如果该路径仍然存在，则会提示让手动输入路径名字（仅仅是目录的名字而不是完整的路径），如果仍然存在，则程序直接退出，具体的路径在脚本运行下载代码结束后的`log`中有说明，同样脚本所要执行的下载命令`log`中也会显示打印出来，如下所示：

```shell
================================================================================================
you have been download LLVM master code
url       : https://gitee.com/openharmony/manifest.git
branch    : master
xml_name  : llvm-toolchain.xml
code_path : /home/wen_fei/OpenHarmony/llvm-master
init   cmd: repo init -u https://gitee.com/openharmony/manifest.git -b master -m llvm-toolchain.xml --no-repo-verify
sync   cmd: repo sync -c
lfs    cmd: repo forall -c 'git lfs pull'
set_br cmd: repo start master --all
================================================================================================

download code success ^_^
```

![icon-note.gif](./img/icon/icon-note.gif) **说明:** 重复路径操作方法

```shell
you have already exist following path:
/home/wen_fei/OpenHarmony/llvm-master
/home/wen_fei/OpenHarmony/llvm-master_tmp
please input the path name you want to download code
if you want to delete the /home/wen_fei/OpenHarmony/llvm-master_tmp directly and then download it again, please press Enter.
llvm_test # 直接输入要存放代码的路径名

================================================================================================
you have been download LLVM master code
url       : https://gitee.com/openharmony/manifest.git
branch    : master
xml_name  : llvm-toolchain.xml
code_path : /home/wen_fei/OpenHarmony/llvm_test
init   cmd: repo init -u https://gitee.com/openharmony/manifest.git -b master -m llvm-toolchain.xml --no-repo-verify
sync   cmd: repo sync -c
lfs    cmd: repo forall -c 'git lfs pull'
set_br cmd: repo start master --all
================================================================================================

download code success ^_^
```

## 二、脚本使用

```shell
git clone https://gitee.com/itopen/ohos_download.git
cd ohos_download
./ohos_download.sh
# 输入所需下载的分支，例如：1.1表示要下载OpenHarmony master分支
```

## 三、问题和建议

因`Gitee`无法评论，大家有什么问题或者建议可以到`OpenHarmony`开发者论坛的下面文章中评论

[如何优雅的一键下载OpenHarmony活跃分支代码？请关注【itopen：ohos_download】 - 文章 OpenHarmony开发者论坛](https://forums.openharmony.cn/forum.php?mod=viewthread&tid=2805&extra=)
